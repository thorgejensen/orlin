
#ifndef  SHORTEST_PATHS_H_
#define  SHORTEST_PATHS_H_
#include <time.h>
#include "2-heap.h"
#include "graph.h"

/*
 * Data structure for returning the results of Dijkstra's algorithm
 * l        - distance values of each vertex
 * p        - predecessor on a shortest path
 * edge     - index of the last edge on a shortest path
 */
typedef struct _DIJKSTRARESULT
{
    double          *l;
    unsigned int    *p;
    unsigned int    *edge;
    clock_t         Insert_time;
    clock_t         DecreaseKey_time;
    clock_t         ExtractMin_time;

}DIJKSTRARESULT, *PDIJKSTRARESULT, **PPDIJKSTRARESULT;

/*
 * CreateDijkstraresult creates an instance of DIJKSTRARESULT for n vertices
 * and returns a pointer to the structure or NULL if allocation of memory fails.
 */
PDIJKSTRARESULT CreateDijkstraresult(unsigned int n);

/*
 * FreeDijkstraresult frees the memory indicated by the passed pointer.
 */
void            FreeDijkstraresult  (PDIJKSTRARESULT);


unsigned int    Dijkstra            (PDIJKSTRARESULT, PHEAP, PGRAPH, unsigned int, unsigned int);
unsigned int    Dijkstra_new        (PDIJKSTRARESULT, PHEAP, PGRAPH, unsigned int, unsigned int);

/*
 * MBF is an implementation of the Moore-Bellman-Ford algorithm that uses the data structure
 * DIJKSTRARESULT for output. This version is used to find a feasible potential and thus
 * simulates the situation that s is connected with all other vertices via edges of cost 0.
 */
void            MBF                 (PDIJKSTRARESULT, PGRAPH);


#endif /*SHORTEST_PATHS_H_*/

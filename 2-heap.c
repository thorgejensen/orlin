
#include <malloc.h>
#include <math.h>
#include "2-heap.h"

PHEAP CreateHeap(unsigned int size)
{
    PHEAP heap = (PHEAP) calloc(1, sizeof(HEAP) );
    /* heap.length = 0; */
    if (!heap)
    {
        return NULL;
    }
    else
    {
        heap->data = (PHEAPNODE) malloc(size * sizeof(HEAPNODE));
        if (!heap->data)
        {
            return NULL;
        }
        else
        {
            heap->reference = (unsigned int*) malloc(size * sizeof(unsigned int));
            if (!heap->reference) return NULL;
        }
    }

    return heap;
}

void FreeHeap(PHEAP heap)
{
    if (heap)
    {
        free(heap->reference);
        free(heap->data);
        free(heap);
    }
}

void HeapifyDown(PHEAP heap)
{
    unsigned int    index = 0, swap_index, tmp_index, left_son_index = 1, father_index, son_index, length = heap->length;
    PHEAPNODE       father_heapnode, son_heapnode;
    HEAPNODE        tmp_heapnode;

    while( left_son_index < length)
    {
        swap_index     = left_son_index;

        /* Find the index of the child with the smallest key */
        tmp_index = left_son_index + 1;
        if( (tmp_index < length) && (heap->data[left_son_index].key > heap->data[tmp_index].key) )
            swap_index = tmp_index;
        else
            swap_index = left_son_index;

        if(heap->data[index].key > heap->data[swap_index].key)
        {
            /* Swap */
            father_heapnode               = &(heap->data[index]);
            son_heapnode                  = &(heap->data[swap_index]);

            tmp_heapnode                  = *father_heapnode;
            father_index                  = son_heapnode->index;
            *father_heapnode              = *son_heapnode;

            son_index                     = tmp_heapnode.index;
            *son_heapnode                 = tmp_heapnode;

            tmp_index                     = heap->reference[father_index];
            heap->reference[father_index] = heap->reference[son_index];
            heap->reference[son_index]    = tmp_index;

            index = swap_index;
            left_son_index = (index << 1) + 1;
        }
        else
            break;
    }
}

void HeapifyUp(PHEAP heap, unsigned int index)
{
    unsigned int    tmp_index, father_index, son_index;
    PHEAPNODE       father_heapnode, son_heapnode;
    HEAPNODE        tmp_heapnode;

    for( son_heapnode =  &(heap->data[index]); index; index = (index - 1) >> 1)
    {
        father_heapnode = &(heap->data[ (index - 1) >> 1 ]);

        if ( son_heapnode->key < father_heapnode->key)
        {
            /* Swap */
            tmp_heapnode                  = *father_heapnode;
            father_index                  = son_heapnode->index;
            *father_heapnode              = *son_heapnode;

            son_index                     = tmp_heapnode.index;
            *son_heapnode                 = tmp_heapnode;

            tmp_index                     = heap->reference[father_index];
            heap->reference[father_index] = heap->reference[son_index];
            heap->reference[son_index]    = tmp_index;

            son_heapnode = father_heapnode;
        }
        else
            break;
    }
}

unsigned int ExtractMin(PHEAP heap)
{
    PHEAPNODE       top             = &(heap->data[0]);
    unsigned int    return_value    = top->index;

    *top                            = heap->data[ --(heap->length) ];
    heap->reference[ top->index ]   = 0;

    HeapifyDown(heap);

    return return_value;
}

void Insert(PHEAP heap, unsigned int index, HEAP_DATATYPE key)
{
    PHEAPNODE       tmp_heapnode;
    unsigned int    length  = heap->length;

    tmp_heapnode            = &(heap->data[ length ]);

    tmp_heapnode->index     = index;
    tmp_heapnode->key       = key;
    heap->reference[index]  = length;

    HeapifyUp(heap, heap->length++ );
}

void DecreaseKey(PHEAP heap, unsigned int index, HEAP_DATATYPE key)
{
    unsigned int heap_index     = heap->reference[index];

    heap->data[heap_index].key  = key;
    HeapifyUp(heap, heap_index);
}

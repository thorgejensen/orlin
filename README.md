# Orlin's Algorithm

Welcome to my implementation of Orlin's algorithm. Orlin's algorithm solves
the general [Minimum Cost Flow Problem](https://en.wikipedia.org/wiki/Minimum-cost_flow_problem)
on networks in O(m*log m (m + n log n)) where n is the number of vertices and m is the 
number of edges. It is based on the following paper:

*  [James B. Orlin - A faster strongly polynomial minimum cost flow algorithm](https://www.math.ubc.ca/~anstee/math523/StronglyPolynomialNetworkFlow.pdf)

This is the fastest known strongly polynomial algorithm for the general Minimum Cost Flow Problem.

It uses standard C. After cloning the repository, you can call the following command
in the repository folder to compile:
```
gcc *.c -lm -o orlin
```

It takes input files in the [DIMACS format for Minimum Cost Flow Problems](http://lpsolve.sourceforge.net/5.5/DIMACS_mcf.htm).
```
./orlin <LIST OF FILES SEPARATED BY SPACES>
```

Instances can for example be created by Netgen, a tool written by Norbert Schlenker.
Netgen is based on the following paper:

* [Klingman, D., A. Napier, and J. Stutz - NETGEN:  A Program for Generating Large Scale Capacitated Assignment, Transportation, and Minimum Cost Flow Network Problems](https://www.jstor.org/stable/2630092?seq=1#metadata_info_tab_contents)

Enjoy playing around with Orlin's algorithm!

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include "shortest_paths.h"
#include "orlin.h"

PDIJKSTRARESULT CreateDijkstraresult(unsigned int n)
{
    PDIJKSTRARESULT result = (PDIJKSTRARESULT) calloc(1, sizeof(DIJKSTRARESULT));

    if (!result)
    {
        return NULL;
    }
    else
    {
        result->l       = (double*)         malloc(n * sizeof(double));
        result->p       = (unsigned int*)   malloc(n * sizeof(unsigned int));
        result->edge    = (unsigned int*)   malloc(n * sizeof(unsigned int));
        if (!result->l || !result->p || !result->edge)
        {
            if(result->l)
                free(result->l);
            if(result->p)
                free(result->p);
            if(result->edge)
                free(result->edge);

            free(result);
            return NULL;
        }
    }

    return result;
}

void FreeDijkstraresult(PDIJKSTRARESULT result)
{
    if (result)
    {
        free(result->l);
        free(result->p);
        free(result->edge);
        free(result);
    }
}

/*
 * Dijkstra control via flag:
 * 0:   dijkstra uses outgoing edges.
 * 1:   dijkstra simulates the ??-graph by using incoming edges as outgoing.
 *
 * And always remember: an edge is called (v,w)
 */

unsigned int Dijkstra(PDIJKSTRARESULT result, PHEAP heap, PGRAPH graph, unsigned int x, unsigned int flag)
{
    clock_t         tmp_time;
    PVERTEX         vertex  = graph->vertex;
    PEDGE           edge    = graph->edge;
    unsigned int    *res_edge = result->edge, *incid_edge;
    unsigned int    n = graph->n + graph->m, s = x, v, w, *p = result->p, return_value = x, i, id;
    double          *l = result->l, b, pot, tmp_value, tmp_b = 0, inf = 2*DBL_MAX;

    // Initialization
    for (i = 0; i < n; i++)
    {
        l[i] = inf;
    }

    tmp_time = clock();
    Insert(heap, x, 0);
    result->Insert_time += clock() - tmp_time;

    if(flag)
    {
        for (l[s] = 0; heap->length > 0; )
        {
            tmp_time    = clock();
            s           = ExtractMin(heap);
            result->ExtractMin_time += clock() - tmp_time;

            pot         = vertex[s].pot;
            incid_edge  = vertex[s].edge;

            for (i = 0; i < vertex[s].incid_edg_cnt; i++)
            {
                id = incid_edge[i];
                if ((w = edge[id].w) == s)
                {
                    // Vorwaertstkante verwenden
                    v = edge[id].v;

                    // Compute distance value using the edge's reduced cost in the backwards graph
                    tmp_value   = l[s] + edge[id].c + vertex[v].pot - pot;
                }
                else if (edge[id].f)
                {
                    // Rueckwaertskante verwenden
                    v = w;
                    w = edge[id].v; // = s;

                    // Compute distance value using the edge's reduced cost in the backwards graph
                    tmp_value   = l[s] - edge[id].c + vertex[v].pot - pot;
                }
                else
                    continue;

                if (l[v] == inf)
                {
                    l[v]        = tmp_value;
                    p[v]        = s;
                    res_edge[v] = id;

                    tmp_time    = clock();
                    Insert(heap, v, tmp_value);
                    result->Insert_time += clock() - tmp_time;
                }
                else if (l[v] > tmp_value)
                {
                    l[v]        = tmp_value;
                    p[v]        = s;
                    res_edge[v] = id;

                    tmp_time    = clock();
                    DecreaseKey(heap, v, tmp_value);
                    result->DecreaseKey_time += clock() - tmp_time;
                }
            }

            b = vertex[s].b;

            // Find the sink cheapest reachable
            //if ( (b > 0) && (graph->gamma / n < b) && (return_value == x))
            if (b > tmp_b)
            {
                return_value = s;
                tmp_b = b;
            }
        }
    }
    else
    {
        for (l[s] = 0; heap->length > 0; )
        {
            tmp_time    = clock();
            s           = ExtractMin(heap);
            result->ExtractMin_time += clock() - tmp_time;

            pot         = vertex[s].pot;
            incid_edge  = vertex[s].edge;

            for (i = 0; i < vertex[s].incid_edg_cnt; i++)
            {
                id = incid_edge[i];
                if ((v = edge[id].v) == s)
                {
                    // Vorwaertstkante verwenden
                    w = edge[id].w;

                    // Compute distance value using the edge's reduced cost in the residual graph
                    tmp_value   = l[s] + edge[id].c + pot - vertex[w].pot;
                }
                else if (edge[id].f)
                {
                    // Rueckwaertskante verwenden
                    w = v;
                    v = edge[id].w; // = s;

                    // Compute distance value using the edge's reduced cost in the residual graph
                    tmp_value   = l[s] - edge[id].c + pot  - vertex[w].pot;
                }
                else
                    continue;

                if (l[w] == inf)
                {
                    l[w]        = tmp_value;
                    p[w]        = s;
                    res_edge[w] = id;

                    tmp_time    = clock();
                    Insert(heap, w, tmp_value);
                    result->Insert_time += clock() - tmp_time;
                }
                else if (l[w] > tmp_value)
                {
                    l[w]        = tmp_value;
                    p[w]        = s;
                    res_edge[w] = id;

                    tmp_time    = clock();
                    DecreaseKey(heap, w, tmp_value);
                    result->DecreaseKey_time += clock() - tmp_time;
                }
            }

            b = vertex[s].b;

            // Find the sink cheapest reachable
            //if ( (b < 0) && (- graph->gamma / n > b) && (return_value == x))
            if (b < tmp_b)
            {
                return_value = s;
                tmp_b = b;
            }
        }
    }
    graph->tmp_pot = l[s];

    if (l[return_value] == inf || return_value == x)
    {
        fprintf(stderr, "The given instance is not solvable!");
        FreeGraph(graph);
        FreeHeap(heap);
        FreeDijkstraresult(result);
        exit(1);
    }

    return return_value;
}

unsigned int Dijkstra_new(PDIJKSTRARESULT result, PHEAP heap, PGRAPH graph, unsigned int x, unsigned int flag)
{
    clock_t         tmp_time;
    PVERTEX         vertex  = graph->vertex;
    PEDGE           edge    = graph->edge;
    unsigned int    *res_edge = result->edge, *incid_edge;
    unsigned int    n = graph->n + graph->m, s = x, v, w, *p = result->p, return_value = x, i, id;
    double          *l = result->l, b, pot, tmp_value, inf = 2*DBL_MAX, tmp_b = 0, tmp = 0;

    for (i = 0; i < n; i++)
    {
        l[i] = inf;
    }

    if(flag)
    {
        //tmp_b = inf;

        // Initialization
        if (x >= graph->n)
        {
            pot         = vertex[x].pot;
            incid_edge  = vertex[x].edge;

            for (i = 0; i < 2; i++) // vertex[x].incid_edg_cnt == 2
            {
                id = incid_edge[i];

                // Rueckwaertskante verwenden
                v = edge[id].w;
                tmp_value = -edge[id].c + vertex[v].pot - pot; // l[x] == 0

                if (edge[id].f)
                {
                    l[v] = tmp_value;
                    p[v] = x;
                    res_edge[v] = id;

                    tmp_time = clock();
                    Insert(heap, v, tmp_value);
                    result->Insert_time += clock() - tmp_time;
                }
            }
        }
        else
        {
            tmp_time = clock();
            Insert(heap, x, 0);
            result->Insert_time += clock() - tmp_time;
        }

        l[s] = 0;
        while (heap->length > 0)
        {
            tmp_time    = clock();
            s           = ExtractMin(heap);
            result->ExtractMin_time += clock() - tmp_time;

            if (l[s] > tmp)
                tmp  = l[s];

            incid_edge = vertex[s].edge;

            b = vertex[s].b;
            // Find the vertex with maximal balance value
            if (b > tmp_b)
            //if (b > 0 && l[s]/b < tmp_b)
            {
                return_value = s;
                tmp_b = b;
            }

            for (i = 0; i < vertex[s].incid_edg_cnt; i++)
            {
                id = incid_edge[i];

                // Vorwaertstkante verwenden
                v = edge[id].v;
                pot = vertex[s].pot; // Potential des Endknotens

                // Compute distance value using the edge's reduced cost in the residual graph
                tmp_value   = l[s] + edge[id].c + vertex[v].pot - pot;

                if (l[v] > tmp_value)
                {
                    l[v]        = tmp_value;
                    p[v]        = s;
                    res_edge[v] = id;
                    if (tmp_value > tmp)
                        tmp = tmp_value;
                }

                b = vertex[v].b;
                // Find the vertex with maximal balance value
                if (b > tmp_b)
                //if (b > 0 && l[v]/b < tmp_b)
                {
                    return_value = v;
                    tmp_b = b;
                }

                // Rueckwaertskante verwenden
                w = v;
                pot = vertex[w].pot; // Potential des Endknotens

                (id % 2) ? id-- : id++;
                v = edge[id].w;

                // Compute distance value using the edge's reduced cost in the residual graph
                tmp_value = l[w] - edge[id].c + vertex[v].pot - pot;

                if (edge[id].f)
                {
                    if (l[v] == inf)
                    {
                        l[v]        = tmp_value;
                        p[v]        = w;
                        res_edge[v] = id;

                        tmp_time    = clock();
                        Insert(heap, v, tmp_value);
                        result->Insert_time += clock() - tmp_time;
                    }
                    else if (l[v] > tmp_value)
                    {
                        l[v]        = tmp_value;
                        p[v]        = w;
                        res_edge[v] = id;

                        tmp_time    = clock();
                        DecreaseKey(heap, v, tmp_value);
                        result->DecreaseKey_time += clock() - tmp_time;
                    }
                }
            }
        }
    }
    else
    {
        //tmp_b = -inf;

        // Initialization
        if (x >= graph->n)
        {
            pot         = vertex[x].pot;
            incid_edge  = vertex[x].edge;

            for (i = 0; i < 2; i++) // vertex[x].incid_edg_cnt == 2
            {
                id = incid_edge[i];

                // Vorwaertstkante verwenden
                w = edge[id].w;
                tmp_value = edge[id].c + pot - vertex[w].pot; // l[x] == 0

                l[w] = tmp_value;
                p[w] = x;
                res_edge[w] = id;

                tmp_time = clock();
                Insert(heap, w, tmp_value);
                result->Insert_time += clock() - tmp_time;
            }
        }
        else
        {
            tmp_time = clock();
            Insert(heap, x, 0);
            result->Insert_time += clock() - tmp_time;
        }

        l[s] = 0;
        while (heap->length > 0)
        {
            tmp_time    = clock();
            s           = ExtractMin(heap);
            result->ExtractMin_time += clock() - tmp_time;

            if (l[s] > tmp)
                tmp = l[s];

            incid_edge = vertex[s].edge;

            b = vertex[s].b;
            // Find the vertex with minimal balance value
            if (b < tmp_b)
            //if (b < 0 && l[s]/b > tmp_b)
            {
                return_value = s;
                tmp_b = b;
            }

            for (i = 0; i < vertex[s].incid_edg_cnt; i++)
            {
                id = incid_edge[i];

                if (edge[id].f)
                {
                    // Rueckwaertskante verwenden
                    w = edge[id].v;
                    pot = vertex[s].pot; // Potential des Anfangknotens

                    // Compute distance value using the edge's reduced cost in the residual graph
                    tmp_value   = l[s] - edge[id].c + pot  - vertex[w].pot;

                    if (l[w] > tmp_value)
                    {
                        l[w]        = tmp_value;
                        p[w]        = s;
                        res_edge[w] = id;

                        if (tmp_value > tmp)
                            tmp  = tmp_value;
                    }

                    b = vertex[w].b;
                    // Find the vertex with minimal balance value
                    if (b < tmp_b)
                    //if (b < 0 && l[w]/b > tmp_b)
                    {
                        return_value = w;
                        tmp_b = b;
                    }

                    // Vorwaertstkante verwenden
                    v = w;
                    pot = vertex[v].pot; // Potential des Anfangsknotens

                    (id % 2) ? id-- : id++;
                    w = edge[id].w;

                    // Compute distance value using the edge's reduced cost in the residual graph
                    tmp_value = l[v] + edge[id].c + pot - vertex[w].pot;

                    if (l[w] == inf)
                    {
                        l[w]        = tmp_value;
                        p[w]        = v;
                        res_edge[w] = id;

                        tmp_time    = clock();
                        Insert(heap, w, tmp_value);
                        result->Insert_time += clock() - tmp_time;
                    }
                    else if (l[w] > tmp_value)
                    {
                        l[w]        = tmp_value;
                        p[w]        = v;
                        res_edge[w] = id;

                        tmp_time    = clock();
                        DecreaseKey(heap, w, tmp_value);
                        result->DecreaseKey_time += clock() - tmp_time;
                    }
                }
            }
        }
    }

    graph->tmp_pot = tmp;

    if (l[return_value] == inf || return_value == x)
    {
        fprintf(stderr, "The given instance is not solvable!");
        FreeGraph(graph);
        FreeHeap(heap);
        FreeDijkstraresult(result);
        exit(1);
    }

    return return_value;
}

void MBF (PDIJKSTRARESULT result, PGRAPH graph)
{
    unsigned int    n = graph->n + graph->m, m = 2 * graph->m, *p = result->p, *res_edge = result->edge, v, w, i, j;
    double          *l = result->l;
    PEDGE           edge = graph->edge;

    /* Initialization: Simulate the situation that s is connected with
     * all other vertices via edges of cost 0. */
    for (i=0; i < n; i++)
    {
        l[i]        = 0;
        p[i]        = n;
        res_edge[i] = 0;
    }

    for(j = 0; j < n; j++)
    {
        for(i = 0; i < m; i++)
        {
            v = edge[i].v;
            w = edge[i].w;

            if (l[w] > l[v] + edge[i].c)
            {
                l[w]            = l[v] + edge[i].c;
                p[w]            = v;
                res_edge[w]     = i;
            }
            if (edge[i].f && l[v] > l[w] - edge[i].c)
            {
                l[v]            = l[w] - edge[i].c;
                p[v]            = w;
                res_edge[v]     = i; // other index for reverse edge??
            }
        }
    }
}


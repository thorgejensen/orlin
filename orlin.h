
#ifndef ORLIN_H_
#define ORLIN_H_

#include "shortest_paths.h"
#include "graph.h"

/*
 * GraphReadin reads an instance of the MIN-COST-FLOW-PROBLEM from a file in
 * DIMACS file format and creates the corresponding graph. If any error occurs
 * while allocating the memory, GraphReadin will return NULL.
 */
PGRAPH          GraphReadin (char const*);

/*
 * PrintResult takes a graph and prints the flow value of all edges via
 * the edge array. Edges with a flow of zero are skipped for simplicity.
 */
void            PrintResult (PGRAPH);

/*
 * FreeGraph takes a graph and frees the used memory.
 */
void            FreeGraph   (PGRAPH);
void            Orlin       (char const*);

/*
 * CheckState is a function designed for debugging. Given the instance's path, the current
 * residual graph, the temporary total excess and the total cost value it checks, whether
 * a computational error occured. In case it did, error messages are printed
 * and the sum of error codes is returned. Error codes:
 * 1:   Wrong B-Value of a vertex
 * 2:   Erroneous feasible potential
 * 4:   Invalid flow value on an edge
 * 8:   total_ex computed incorrectly
 * 16:  total_cost computed incorrectly
 * 32:  Erroneous computation of the number of edges with a positive flow value that are not included in F
 *
 * In this function the index of edges and nodes refer to the position in the corresponding
 * array.
 */
unsigned int    CheckState  (const char *, PGRAPH);

/*
 * CheckResult takes a residual graph and checks whether the corresponding b-flow
 * is valid and optimum. It reconstructs the original graph by reading in the information
 * about the instance from a file in the DIMACS file format. The function uses the
 * Moore-Bellman-Ford algorithm to find a feasible potential in the given residual graph
 * and thus needs a pointer to an instance of DIJKSTRARESULT. In addition, the function
 * checks whether the cost of the b-flow was computed correctly.
 *
 * Error indicators:
 * 1:   Flow value on an edge exceeds capacity or is negative
 * 2:   B-flow is not optimum
 * 4:   Flow on an invalid edge
 * 8:   No valid b-flow
 * 16:  Erroneous computation of b-flow's cost
 *
 * The function returns the sum of the error indicators. In this function the index
 * of edges and nodes refer to the position in the corresponding array.
 */
unsigned int    CheckResult (PGRAPH, PDIJKSTRARESULT, char const *, double);

#endif /* ORLIN_H_ */

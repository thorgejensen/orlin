
#ifndef HEAP_H_
#define HEAP_H_

#ifdef  UINT
#define HEAP_DATATYPE   unsigned int
#elif   INT
#define HEAP_DATATYPE   int
#elif   LONG
#define HEAP_DATATYPE   long
#elif   ULONG
#define HEAP_DATATYPE   unsigned long
#elif   FLOAT
#define HEAP_DATATYPE   float
#else
#define HEAP_DATATYPE   double
#endif

typedef struct _HEAPNODE
{
    unsigned int    index;
    HEAP_DATATYPE   key;
}HEAPNODE, *PHEAPNODE, **PPHEAPNODE;

typedef struct _HEAP
{
    unsigned int length;
    unsigned int *reference;
    PHEAPNODE    data;
}HEAP, *PHEAP, **PPHEAP;


/*
 * CreateHeap creates an instance of HEAP for n vertices and returns
 * a pointer to the structure or NULL if allocation of memory fails.
 */
PHEAP           CreateHeap  (unsigned int);

/*
 * FreeHeap frees the memory needed for a heap indicated by the
 * passed pointer.
 */
void            FreeHeap    (PHEAP);

/*
 * HeapifyDown is a function to restore the heap conditions
 * after extracting the minimum.
 */
void            HeapifyDown (PHEAP);

/*
 * HeapifyUp is a function to restore the heap conditions after
 * inserting or decreasing a key.
 */
void            HeapifyUp   (PHEAP, unsigned int);

/*
 * ExtractMin extracts the vertex with minimum key from the heap and
 * returns its id.
 */
unsigned int    ExtractMin  (PHEAP);

/*
 * Insert adds a vertex with given id and key to the Fibonacci heap.
 */
void            Insert      (PHEAP, unsigned int, HEAP_DATATYPE);

/*
 * DecreaseKey decreases the key of the vertex indicated by id to
 * the passed key.
 */
void            DecreaseKey (PHEAP, unsigned int, HEAP_DATATYPE);

#endif /*HEAP_H_*/

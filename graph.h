
#ifndef GRAPH_H_
#define GRAPH_H_

#include <float.h>

typedef struct _EDGE
{
    unsigned int    v;
    unsigned int    w;
    //unsigned int    id;
    double          f;
    double          c;
    unsigned int    in_F; // 1 means in F, 0 means not in F
}EDGE, *PEDGE, **PPEDGE;

typedef struct _VERTEX
{
    double          b;
    double          pot;
    unsigned int    *edge;
    unsigned int    incid_edg_cnt;
}VERTEX, *PVERTEX, **PPVERTEX;

typedef struct _GRAPH
{
    PEDGE           edge;
    PVERTEX         vertex;
    unsigned int    *source;
    unsigned int    *sink;
    unsigned int    *r;
    unsigned int    *Fedge;
    unsigned int    n;      // vertex cnt of the original graph
    unsigned int    m;      // edge cnt of the original graph
    unsigned int    neg_edge_cnt;
    //unsigned int  source_cnt;
    //unsigned int  sink_cnt;
    unsigned int    non_zero_f_cnt; //counts the edges that are not in F and have f <> 0
    double          total_ex;
    double          total_cost;
    double          gamma;
    double          tmp_pot;
}GRAPH, *PGRAPH, **PPGRAPH;

#endif /* GRAPH_H_ */

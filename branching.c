
unsigned int Find (unsigned int* branching, unsigned int v)
{
    unsigned int w;

    if (branching[v] == v)
    {
        return v;
    }
    else
    {
        w               = Find(branching, branching[v]);
        branching[v]    = w;

        return w;
    }
}

void Union (unsigned int* branching, unsigned int v, unsigned int w)
{
    branching[v] =  w;
}


#include <float.h>
#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "orlin.h"
#include "branching.h"

#define OUTFILE "orlin_times.out"

#define BUFSIZE 256

PGRAPH GraphReadin(char const *path)
{
    unsigned int n, m, id, i = 0, v, w, nm, neg_edge_cnt = 0, sink_source = 0, error = 0;
    double   b, l, u, c, sum = 0, total_ex=0;
    FILE         *f;
    char         buffer[BUFSIZE], *str;
    PGRAPH       graph;

    if (!(f = fopen(path, "r")))
    {
        printf("Error while opening file: %s", path);
        exit(1);
    }

    if (( graph = (PGRAPH) calloc(1, sizeof(GRAPH)) ))
    {
        while (!feof(f))
        {
            if( (str = fgets(buffer, BUFSIZE, f)) )
            {
                if (buffer[0] == 'p')
                {
                    if (sscanf(buffer, "p min %u %u", &n, &m) != 2)
                    {
                        fprintf(stderr, "Given file is not in DIMACS-format or an error occured while reading file: %s", path);
                        if (graph->vertex)
                            free(graph->vertex);
                        if (graph->edge)
                            free(graph->edge);
                        free(graph);
                        fclose(f);
                        exit(1);
                    }
                    else
                    {
                        if (graph->vertex && graph->edge)
                        {
                            fprintf(stderr, "Input file should contain only one line describing the task!");
                            free(graph->vertex);
                            free(graph->edge);
                            free(graph);
                            fclose(f);
                            exit(1);
                        }
                        else
                        {
                            nm          = n + m;
                            // save vertex and edge count of the original graph
                            graph->n    = n;
                            graph->m    = m;

                            graph->vertex   = (PVERTEX) calloc(nm, sizeof(VERTEX));
                            graph->edge     = (PEDGE)   calloc(2 * m, sizeof(EDGE));

                        }
                    }
                }
                else if (buffer[0] == 'n' && graph->vertex)
                {
                    if (sscanf(buffer, "n %u %lf", &id, &b) != 2)
                    {
                        fprintf(stderr, "Given file is not in DIMACS-format or an error occured while reading file: %s", path);
                        free(graph->vertex);
                        free(graph->edge);
                        free(graph);
                        fclose(f);
                        exit(1);
                    }
                    else
                    {
                        graph->vertex[id-1].b = b;
                        sum += b;
                    }

                }
                else if (buffer[0] == 'a' && graph->vertex && graph->edge)
                {
                    if (sscanf(buffer, "a %u %u %lf %lf %lf", &v, &w, &l, &u, &c) != 5)
                    {
                        fprintf(stderr, "Given file is not in DIMACS-format or an error occured while reading file: %s", path);
                        free(graph->vertex);
                        free(graph->edge);
                        free(graph);
                        fclose(f);
                        exit(1);
                    }
                    else
                    {
                        if (u > 0)
                        {
                            graph->vertex[n + i].b = u;
                            if (u > graph->gamma)
                                graph->gamma = u;
                            graph->vertex[v - 1].b -= u;

                            total_ex += abs(u);

                            graph->edge[2*i].v      = n + i;
                            graph->edge[2*i].w      = v - 1;
                            //graph->edge[2*i].id   = 2*i;
                            graph->edge[2*i].c      = 0;

                            graph->edge[2*i + 1].v  = n + i;
                            graph->edge[2*i + 1].w  = w - 1;
                            //graph->edge[2*i + 1].id   = 2*i + 1;
                            graph->edge[2*i + 1].c  = c;

                            graph->vertex[n + i].incid_edg_cnt += 2;
                            graph->vertex[v - 1].incid_edg_cnt++;
                            graph->vertex[w - 1].incid_edg_cnt++;

                            if (c < 0)
                                neg_edge_cnt++;
                        }
                        i++;
                    }
                }
                else if (buffer[0] == 'c')
                {
                    while (str && strlen(str) == BUFSIZE-1 && buffer[BUFSIZE-2] != '\n')
                    {
                         str = fgets(buffer, BUFSIZE, f);
                    }
                }
                else if (buffer[0] == '\t' || buffer[0] == ' ')
                {
                    while (str && strlen(str) == BUFSIZE-1 && buffer[BUFSIZE-2] != '\n')
                    {
                        for (i = 0; i < BUFSIZE-1; i++)
                             if (str[i] != '\t' && str[i] != ' ')
                                 error = 1;
                        str = fgets(buffer, BUFSIZE, f);
                    }

                    for (i = 0; i < strlen(str) - 1; i++)
                        if (str[i] != '\t' && str[i] != ' ')
                            error = 1;

                    if (str[strlen(str)-1] != '\n')
                        error = 1;

                    if (error)
                    {
                        fprintf(stderr, "Given file is not in DIMACS-format: %s", path);
                        if (graph->vertex)
                            free(graph->vertex);
                        if (graph->edge)
                            free(graph->edge);
                        free(graph);
                        fclose(f);
                        exit(1);
                    }
                }
                else if (buffer[0] == '\n')
                    ;
                else
                {
                    fprintf(stderr, "Given file is not in DIMACS-format: %s", path);
                    free(graph->vertex);
                    free(graph->edge);
                    free(graph);
                    fclose(f);
                    exit(1);
                }
            }
        }

        graph->r        = (unsigned int*) calloc(n + m, sizeof(unsigned int));
        graph->Fedge    = (unsigned int*) calloc(n + m, sizeof(unsigned int));

        if (!graph->r || !graph->Fedge)
        {
            if (graph->r)
                free(graph->r);
            if (graph->Fedge)
                free(graph->Fedge);
            for(; i; i--)
                free(graph->vertex[--i].edge);
            free(graph->vertex);
            free(graph->edge);
            free(graph);
            fclose(f);
            return NULL;
        }

        for(i = 0; i < nm; i++)
        {
            if (i < n)
            {
                total_ex += abs(graph->vertex[i].b);
                if (abs(graph->vertex[i].b) > graph->gamma)
                    graph->gamma = abs(graph->vertex[i].b);
            }

            graph->vertex[i].edge = (unsigned int*) malloc(graph->vertex[i].incid_edg_cnt * sizeof(unsigned int));
            graph->vertex[i].incid_edg_cnt = 0;

            if (!graph->vertex[i].edge)
            {
                for (; i; )
                    free(graph->vertex[--i].edge);
                free(graph->vertex);
                free(graph->edge);
                free(graph);
                fclose(f);
                return NULL;
            }

            if (graph->vertex[i].b)
                sink_source++;

            graph->r[i] = i;
        }

        graph->sink     = (unsigned int*) calloc(sink_source, sizeof(unsigned int));
        graph->source   = (unsigned int*) calloc(sink_source, sizeof(unsigned int));

        if( !graph->sink || !graph->source)
        {
            if (graph->sink)
                free(graph->sink);
            if (graph->source)
                free(graph->source);

            free(graph->r);
            free(graph->Fedge);
            for(; i; )
                free(graph->vertex[--i].edge);
            free(graph->vertex);
            free(graph->edge);
            free(graph);
            fclose(f);
            return NULL;
        }

        for(i = 0; i < 2 * m; i++)
        {
            if (graph->edge[i].v || graph->edge[i].w)
            {
                v = graph->edge[i].v;
                w = graph->edge[i].w;
                graph->vertex[v].edge[ graph->vertex[v].incid_edg_cnt++ ] = i;
                graph->vertex[w].edge[ graph->vertex[w].incid_edg_cnt++ ] = i;
            }
        }


        graph->total_ex = total_ex / 2;
        graph->neg_edge_cnt = neg_edge_cnt;

        fclose(f);
    }
    else
    {
        fclose(f);
        return NULL;
    }

    if (sum != 0)
    {
        fprintf(stderr, "The given instance is not solvable (sum of balance-values is not zero)!");
        if (graph)
            FreeGraph(graph);
        exit(1);
    }
    else
        return graph;
}

void PrintResult(PGRAPH graph)
{
    unsigned int i, m = 2 * graph->m;

    //Output
    for (i = 1; i < m; i += 2)
    {
        if(graph->edge[i].f)
            printf("%d \t %.2f\n", i/2 + 1, graph->edge[i].f);
    }
}

void FreeGraph(PGRAPH graph)
{
    unsigned int i, n = graph->n + graph->m;

    //Free
    if (graph)
    {
        free(graph->edge);
        for (i = 0; i < n; i++)
        {
            free((graph->vertex[i]).edge);
        }
        free(graph->vertex);
        free(graph->sink);
        free(graph->source);
        free(graph->Fedge);
        free(graph->r);
        free(graph);
    }
}

/*
 * See Dijkstra for flag values!
 */
void Augment(PGRAPH graph, PDIJKSTRARESULT result, unsigned int s, unsigned int t, unsigned int flag)
{
    unsigned int    j, id, *p = result->p, *res_edge = result->edge;
    double          gamma   = graph->gamma, total_cost = 0, tmp_value = 0;
    PEDGE           edge    = graph->edge;
    PVERTEX         vertex  = graph->vertex;

    if(flag)
    {
        for (j = s; j != t; j = p[j])
        {
            id = res_edge[j];
            if(edge[id].v == j)
            {
                // Vorwaertskante im Originalgraphen benutzen
                edge[id].f += gamma;
                total_cost += gamma * edge[id].c;

                if (!edge[id].in_F && edge[id].f == gamma)
                    graph->non_zero_f_cnt++;
            }
            else
            {
                // Rueckwaertskante im Originalgraphen benutzen
                edge[id].f -= gamma;
                total_cost -= gamma * edge[id].c;

                if (!edge[id].in_F && !edge[id].f)
                        graph->non_zero_f_cnt--;
            }
        }
    }
    else
    {
        for (j = t; j != s; j = p[j])
        {
            id = res_edge[j];
            if(edge[id].w == j)
            {
                // Vorwaertskante im Originalgraphen benutzen
                edge[id].f += gamma;
                total_cost += gamma * edge[id].c;

                if (!edge[id].in_F && edge[id].f == gamma)
                    graph->non_zero_f_cnt++;
            }
            else
            {
                // Rueckwaertskante im Originalgraphen benutzen
                edge[id].f -= gamma;
                total_cost -= gamma * edge[id].c;

                if (!edge[id].in_F && !edge[id].f)
                    graph->non_zero_f_cnt--;
            }
        }
    }

    if (vertex[s].b > gamma)
        tmp_value -= gamma;
    else
        tmp_value -= vertex[s].b;

    if (fabs(vertex[t].b) <= gamma)
        tmp_value += gamma + vertex[t].b;

    graph->total_ex += tmp_value;
    graph->total_cost += total_cost;

    vertex[s].b -= gamma;
    vertex[t].b += gamma;
}

void Add_edge_to_F(PGRAPH graph, unsigned int i, unsigned int x, unsigned int y, unsigned int r_x, unsigned int r_y, double augm_val, double value)
{
    unsigned int    v, w, id, id_tmp;
    unsigned int    *Fedge = graph->Fedge;
    double          total_cost = 0;

    PEDGE           edge = graph->edge;

    for (v = x, id = Fedge[v]; v != r_x; id = id_tmp)
    {
        if (edge[id].v == v)
        {
            // Rueckwaertskante benutzen
            edge[id].f -= augm_val;
            total_cost -= augm_val * edge[id].c;

            w           = edge[id].w;
            id_tmp      = Fedge[w];
            Fedge[w]    = id;
            v           = w;

            if (!edge[id].in_F && edge[id].f == value)
                graph->non_zero_f_cnt--;
        }
        else
        {
            if (!edge[id].in_F && edge[id].f == value)
                graph->non_zero_f_cnt++;

            // Vorwaertskante benutzen
            edge[id].f += augm_val;
            total_cost += augm_val * edge[id].c;

            v           = edge[id].v;
            id_tmp      = Fedge[v];
            Fedge[v]    = id;
        }
    }

    Fedge[x]        = i; // Rueckwaertskante im Branching
    edge[i].in_F    = 1;
    edge[i].f      += augm_val;
    total_cost     += augm_val * edge[i].c;
    graph->non_zero_f_cnt--;

    for (v = y, id = Fedge[v]; v != r_y; id = Fedge[v])
    {
        if (edge[id].v == v)
        {
            if (!edge[id].in_F && edge[id].f == value)
                graph->non_zero_f_cnt++;

            // Vorwaertskante benutzen, aber Rueckwaertskante im Branching
            edge[id].f += augm_val;
            total_cost += augm_val * edge[id].c;

            v = edge[id].w;
        }
        else
        {
            // Rueckwaertskante benutzen, aber Vorwaertskante im Branching
            edge[id].f -= augm_val;
            total_cost -= augm_val * edge[id].c;

            if (!edge[id].in_F && edge[id].f == value)
                graph->non_zero_f_cnt--;

            v = edge[id].v;
        }
    }

    graph->total_cost += total_cost;
}



void Orlin(char const* path)
{
    clock_t         start, dijkstra_start;
    unsigned int    s = 0, t = 0, i, j, k, id, x, y, r_x, r_y;
    unsigned int    n, m, *source, *sink, source_cnt = 0, sink_cnt = 0;
    unsigned int    *Fedge, *r;
    unsigned int    dijkstra_cnt = 0, phase_cnt = 0;
    int             min = 0;
    double          gamma, gamma_part, augm_val, tmp_max, tmp_value;
    double          total_time, dijkstra_time = 0;
    double          inf = 2 * DBL_MAX;

    FILE            *f;
    PDIJKSTRARESULT result = NULL;
    PHEAP           heap = NULL;
    PGRAPH          graph  = GraphReadin(path);
    PVERTEX         vertex;
    PEDGE           edge;

    if (graph)
    {
        // heap     = CreateHeap(graph->m + graph->n); // for old Dijkstra
        heap    = CreateHeap(graph->n);
        result  = CreateDijkstraresult(graph->m + graph->n);
    }


    if (result && heap && (f = fopen(OUTFILE, "a")))
    {
        start = clock();
        //fprintf(f, "File: %s\n", path);
        //fprintf(f, "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

        n           = graph->n;
        m           = graph->m;
        vertex      = graph->vertex;
        edge        = graph->edge;

        sink        = graph->sink;
        source      = graph->source;

        Fedge       = graph->Fedge;
        r           = graph->r;

        gamma       = graph->gamma;

        // Find feasible potential in O(m)
        if (graph->neg_edge_cnt)
        {
            for (i = 0; i < n; i++)
            {
                min = 0;

                for (j = 0; j < vertex[i].incid_edg_cnt; j++)
                {
                    id = vertex[i].edge[j];
                    if (edge[id].c < min)
                        min = edge[id].c;
                }

                vertex[i].pot = -min;
            }
        }

        // Find important sources and sinks
        gamma_part = ( (double)(n + m - 1) )/(n + m) * gamma;

        for (i = 0; i < n; i++)
        {
            if(vertex[i].b < -gamma_part)
                sink[ sink_cnt++ ] = i;
        }
        for (i = n; i < n + m; i++)
        {
            if (vertex[i].b > gamma_part)
            {
                source[ source_cnt++ ] = i;
            }
        }

        while ( graph->total_ex )
        {
            i = 0;
            j = 0;
            while (j < sink_cnt || i < source_cnt)
            {
                for(;i < source_cnt; i++)
                {
                    if (vertex[ source[i] ].b > gamma_part)
                        break;
                }

                if (i != source_cnt)
                {
                    s = source[i];

                    dijkstra_start = clock();
                    t = Dijkstra_new(result, heap, graph, s, 0);
                    dijkstra_time += (double)(clock() - dijkstra_start);
                    dijkstra_cnt++;

                    //printf("%u. Augmentierungsschritt: Quelle %u (b-Wert: %f), Senke %u (b-Wert:%f) (Vorwaertsdijkstra) \n", dijkstra_cnt, s, vertex[s].b, t, vertex[t].b);
                    //fprintf(f, "%u. Augmentierungsschritt: Quelle %u (b-Wert: %f), Senke %u (b-Wert:%f) (Vorwaertsdijkstra)\n", dijkstra_cnt, s, vertex[s].b, t, vertex[t].b);

                    Augment(graph, result, s, t, 0);

                    // Update potential
                    for (k = 0; k < n + m; k++)
                    {
                        if (result->l[k] < inf)
                            vertex[k].pot += result->l[k];
                        else
                            vertex[k].pot += graph->tmp_pot;
                    }
                }
                else
                {
                    for(;j < sink_cnt; j++)
                    {
                        if (vertex[ sink[j] ].b < -gamma_part)
                            break;
                    }

                    if (j != sink_cnt)
                    {
                        t = sink[j];

                        dijkstra_start = clock();
                        s = Dijkstra_new(result, heap, graph, t, 1);
                        dijkstra_time += (double)(clock() - dijkstra_start);
                        dijkstra_cnt++;

                        //printf("%u. Augmentierungsschritt: Quelle %u (b-Wert: %f), Senke %u (b-Wert:%f) (Rueckwaertsdijkstra)\n", dijkstra_cnt, s, vertex[s].b, t, vertex[t].b);
                        //fprintf(f, "%u. Augmentierungsschritt: Quelle %u (b-Wert: %f), Senke %u (b-Wert: %f) (Rueckwaertsdijkstra)\n", dijkstra_cnt, s, vertex[s].b, t, vertex[t].b);

                        Augment(graph, result, s, t, 1);

                        // Update potential
                        for (k = 0; k < n + m; k++)
                        {
                            if (result->l[k] < inf)
                                vertex[k].pot -= result->l[k];
                            else
                                vertex[k].pot -= graph->tmp_pot;
                        }
                    }
                }
            }

            /* Schritt 6*/
            if (!graph->non_zero_f_cnt)
            {
                tmp_max = 0;

                for (i = 0; i < m + n; i++)
                {
                    if (tmp_max < fabs(vertex[i].b))
                        tmp_max = fabs(vertex[i].b);
                }

                if (gamma / 2 < tmp_max)
                    gamma /= 2;
                else
                    gamma = tmp_max;
            }
            else
                gamma /= 2;

            graph->gamma = gamma;
            phase_cnt++;
            //printf("Gamma-Wert in der %u. Phase: %f\n", phase_cnt, gamma);
            //fprintf(f, "Gamma-Wert in der %u. Phase: %f\n", phase_cnt, gamma);

            /* Schritt 7*/
            for (i = 0; i < 2 * m; i++)
            {
                x = edge[i].v;
                y = edge[i].w;
                r_x = Find(r, x);
                r_y = Find(r, y);

                if (edge[i].in_F || r_x == r_y || edge[i].f <= 8 * (n + m) * gamma)
                    continue;

                augm_val = vertex[r_x].b;
                tmp_value = 0;

                if (augm_val > 0)
                {
                    Add_edge_to_F(graph, i, x, y, r_x, r_y, augm_val, 0);

                    if (vertex[r_y].b < 0)
                    {
                        if (fabs(vertex[r_y].b) > augm_val)
                            tmp_value = -augm_val;
                        else
                            tmp_value = vertex[r_y].b;
                    }
                }
                else
                {
                    Add_edge_to_F(graph, i, x, y, r_x, r_y, augm_val, augm_val);

                    if (vertex[r_y].b > 0)
                    {
                        if (vertex[r_y].b > -augm_val)
                            tmp_value = augm_val;
                        else
                            tmp_value = -vertex[r_y].b;
                    }
                }

                vertex[r_y].b += augm_val;
                vertex[r_x].b  = 0;

                graph->total_ex += tmp_value;

                Union(r, r_x, r_y);
            }

            /* Find important sinks and sources again   */
            sink_cnt    = 0;
            source_cnt  = 0;
            gamma_part = ( (double)(n + m - 1) ) / (n + m) * gamma;
            for (i = 0; i < n + m; i++)
            {
                if (vertex[i].b > gamma_part)
                    source[ source_cnt++ ] = i;
                else if (vertex[i].b < -gamma_part)
                    sink[ sink_cnt++ ]    = i;
            }
        }

        total_time = (double)(clock()-start)/CLOCKS_PER_SEC;
        printf("%.3f\n", graph->total_cost);

        //CheckResult(graph, result, path, graph->total_cost);
        PrintResult(graph);

        printf("\nTime elapsed:\n\tOrlin: %.4f sec. (Ignoring malloc and free operations)\n", total_time);
        printf("\tDijkstra: %.4f sec. (%.4f %% of total time)\n", (double)dijkstra_time / CLOCKS_PER_SEC, (double) dijkstra_time / CLOCKS_PER_SEC / total_time * 100);

        printf("\tExplicitly:\t");
        printf("ExtractMin: \t%.4f sec. (%.4f%% of Dijkstra)\n", (double)(result->ExtractMin_time) / CLOCKS_PER_SEC, (double)(result->ExtractMin_time) / CLOCKS_PER_SEC / total_time * 100);
        printf("\t\t\tDecreaseKey: \t%.4f sec. (%.4f%% of Dijkstra)\n", (double)(result->DecreaseKey_time) / CLOCKS_PER_SEC, (double)(result->DecreaseKey_time) / CLOCKS_PER_SEC / total_time * 100);
        printf("\t\t\tInsert: \t%.4f sec. (%.4f%% of Dijkstra)\n", (double)(result->Insert_time) / CLOCKS_PER_SEC, (double)(result->Insert_time) / CLOCKS_PER_SEC / total_time * 100);

        printf("Anzahl an Dijkstra-Aufrufen: %u \n", dijkstra_cnt);
        printf("Anzahl an Phasen: %u \n", phase_cnt);

        fprintf(f, "Result: %.3f\n", graph->total_cost);
        fprintf(f, "Time elapsed:\n\tOrlin: %.4f sec. (Ignoring malloc and free operations)\n", total_time);
        fprintf(f, "\tDijkstra: %.4f sec. (%.4f %% of total time)\n", (double)dijkstra_time / CLOCKS_PER_SEC, (double) dijkstra_time / CLOCKS_PER_SEC / total_time * 100);
        fprintf(f, "\tExplicitly:\t");
        fprintf(f, "ExtractMin: \t%.4f sec. (%.4f%% of Dijkstra)\n", (double)(result->ExtractMin_time) / CLOCKS_PER_SEC, (double)(result->ExtractMin_time) / CLOCKS_PER_SEC / total_time * 100);
        fprintf(f, "\t\t\tDecreaseKey: \t%.4f sec. (%.4f%% of Dijkstra)\n", (double)(result->DecreaseKey_time) / CLOCKS_PER_SEC, (double)(result->DecreaseKey_time) / CLOCKS_PER_SEC / total_time * 100);
        fprintf(f, "\t\t\tInsert: \t%.4f sec. (%.4f%% of Dijkstra)\n", (double)(result->Insert_time) / CLOCKS_PER_SEC, (double)(result->Insert_time) / CLOCKS_PER_SEC / total_time * 100);
        fprintf(f, "Anzahl an Dijkstra-Aufrufen: %u\n", dijkstra_cnt);
        fprintf(f, "Anzahl an Phasen: %u\n", phase_cnt);
        fprintf(f, "\n");
        fclose(f);

        FreeGraph(graph);
        FreeHeap(heap);
        FreeDijkstraresult(result);
    }
    else
    {
        fprintf(stderr, "Error while allocating memory!");
        if (result)
            FreeDijkstraresult(result);
        if (heap)
            FreeHeap(heap);
        if (graph)
            FreeGraph(graph);
        if (f)
            fclose(f);
        exit(2);
    }
}

/*
 * Error codes:
 * 1:   Falscher B-Wert eines Knotens
 * 2:   Unzulaessiges Potential
 * 4:   Ungueltiger Flusswert auf einer Kante
 * 8:   total_ex falsch berechnet
 * 16:  Kostenwert falsch berechnet
 * 32:  Anzahl an Kanten ausserhalb von F mit positivem Fluss falsch berechnet
 *
 * Die Nummerierung der Kanten und Knoten bezieht sich in dieser Methode auf ihre Position
 * im entsprechenden Array.
 */
unsigned int CheckState(const char* path, PGRAPH graph)
{
    unsigned int    n = graph->n + graph->m, m = 2 * graph->m, non_zero_f_cnt = 0, status = 0;
    unsigned int    id, v, w, i, j;
    double          total_ex = 0, total_cost = 0;
    double          gamma = graph->gamma, b;

    PGRAPH          new_graph = GraphReadin(path);
    PVERTEX         vertex = graph->vertex;
    PEDGE           edge = graph->edge;

    if (new_graph)
    {
        for (i = 0; i < n; i++)
        {
            if (vertex[i].b > 0)
                total_ex += vertex[i].b;

            b = new_graph->vertex[i].b;
            for (j = 0; j < vertex[i].incid_edg_cnt; j++)
            {
                id = vertex[i].edge[j];
                if (edge[id].v == i)
                {
                    b -= edge[id].f;
                }
                else
                {
                    b += edge[id].f;
                }
            }

            if (b != vertex[i].b)
            {
                fprintf(stderr, "FALSCHER B-WERT: Knoten %u sollte %.3f haben, hat allerdings %.3f\n", i, b, vertex[i].b);
                if (!status) status++;
            }

            if (fabs(vertex[i].b) > (n - 1.)/n * gamma)
                fprintf(stderr, "WICHTER KNOTEN GEFUNDEN: %u (b-Wert: %f)\n", i, vertex[i].b);
        }

        for (i = 0; i < m; i++)
        {
            v           = edge[i].v;
            w           = edge[i].w;
            total_cost += edge[i].f * edge[i].c;

            if (!edge[i].in_F && edge[i].f)
                non_zero_f_cnt++;

            if (edge[i].c + vertex[v].pot - vertex[w].pot < 0)
            {
                fprintf(stderr, "UNZULAESSIGES POTENTIAL: Kante %u hat negative reduzierte Kosten\n", i);
                if (status % 4 == 1 || status % 4 == 0) status += 2;
            }

            if (edge[i].f && -edge[i].c + vertex[w].pot - vertex[v].pot < 0)
            {
                fprintf(stderr, "UNZULAESSIGES POTENTIAL: Rueckwaertskante %u hat negative reduzierte Kosten\n", i);
                if (status % 4 == 1 || status % 4 == 0) status += 2;
            }

            if (!edge[i].in_F && gamma && floor(edge[i].f / gamma + 0.5) != edge[i].f / gamma)
            {
                fprintf(stderr, "UNGUELTIGER FLUSSWERT AUF EINER KANTE: Kante %u hat Fluss %f, was kein ganzzahliges Vielfaches von %f ist\n", i, edge[i].f, gamma);
                if (status < 4) status += 4;
            }
        }

        if (total_ex != graph->total_ex)
        {
            fprintf(stderr, "FALSCHER WERT VON TOTAL_EX: %.3f, sollte allerdings %.3f sein\n", graph->total_ex, total_ex);
            status += 8;
        }

        if (total_cost != graph->total_cost)
        {
            fprintf(stderr, "FALSCHER KOSTENWERT: %.3f, sollte allerdings %.3f sein\n", graph->total_cost, total_cost);
            status += 16;
        }

        if (non_zero_f_cnt != graph->non_zero_f_cnt)
        {
            fprintf(stderr, "FALSCHE ANZAHL DER KANTEN AUSSERHALB VON F MIT POSITIVEM FLUSS: %u, sollte allerdings %u sein\n", graph->non_zero_f_cnt, non_zero_f_cnt);
            status += 32;
        }

        FreeGraph(new_graph);
    }
    else
    {
        fprintf(stderr, "Fehler beim Alloziieren des Speichers!\n");
        exit(2);
    }

    return status;
}


/*
 * Error codes:
 * 1:   Ungueltiger Flusswert auf einer Kante
 * 2:   B-Fluss nicht optimal
 * 4:   Fluss auf einer ungueltigen Kante
 * 8:   Kein zulaessiger b-Fluss
 * 16:  Kostenwert falsch berechnet
 *
 * Die Nummerierung der Kanten und Knoten bezieht sich in dieser Methode auf ihre Position
 * im entsprechenden Array.
 */
unsigned int CheckResult(PGRAPH res_graph, PDIJKSTRARESULT result, char const *path, double value)
{
    unsigned int    m = res_graph->m, n = res_graph->n, status = 0, v, w, i;
    double          *b = (double*)calloc(n+m, sizeof(double)), total_cost = 0;
    PGRAPH          graph = GraphReadin(path);
    PVERTEX         vertex;
    PEDGE           edge;

    if (b && graph && m == graph->m && n == graph->n)
    {
        vertex = graph->vertex;
        edge = res_graph->edge;

        // b-Werte der Knoten und Optimalitaet des Flusses ueberpruefen
        MBF(result, res_graph);

        for (i = 0; i < 2*m; i++)
        {
            v = edge[i].v;
            w = edge[i].w;

            if (edge[i].c + result->l[v] - result->l[w] < 0 && (status % 4 == 1 || status % 4 == 0))
            {
                fprintf(stderr, "B-FLUSS NICHT OPTIMAL\n");
                status += 2;
            }

            if (edge[i].f && -edge[i].c + result->l[w] - result->l[v] < 0 && (status % 4 == 1 || status % 4 == 0))
            {
                fprintf(stderr, "B-FLUSS NICHT OPTIMAL\n");
                status += 2;
            }

            b[v] += edge[i].f;
            b[w] -= edge[i].f;

            total_cost += edge[i].f * edge[i].c;

            if (i % 2 == 1 && v != w && (edge[i].f < 0  || edge[i].f > vertex[v].b))
            {
                fprintf(stderr, "UNGUELTIGER FLUSSWERT AUF DER KANTE %u\n", i);
                if (status % 2 == 0) status++;
            }

            if (!v && !w && edge[i].f)
            {
                fprintf(stderr, "FLUSS AUF EINER UNGUELTIGEN KANTE %u", i);
                if (status < 4) status += 4;

            }
        }

        for (i = 0; i < n+m; i++)
        {
            if (b[i] != graph->vertex[i].b)
            {
            fprintf(stderr, "KEIN ZULAESSIGER B-FLUSS: Knoten %u hat b-Wert %f, sollte allerdings %f haben\n", i, b[i], graph->vertex[i].b);
                if (status < 8) status += 8;
            }
        }

        if (total_cost != value)
        {
            fprintf(stderr, "FALSCHER KOSTENWERT: Ergebnis ist %.3f, sollte allerdings %.3f sein\n", value, total_cost);
            status += 16;
        }

        FreeGraph(graph);
        free(b);
    }
    else
    {
        fprintf(stderr, "Fehler beim Alloziieren des Speichers oder unterschiedliche Knoten- bzw. Kantenzahl der zu vergleichenden Graphen\n");
        if (b)
            free(b);
        if (graph)
            FreeGraph(graph);
        exit(2);
    }

    return status;
}

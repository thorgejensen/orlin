
#ifndef BRANCHING_H_
#define BRANCHING_H_

/*
 * A not very sophisticated implementation of a
 * Union-Find-Branching with path-compression.
*/

unsigned int Find (unsigned int*, unsigned int);
void Union (unsigned int*, unsigned int, unsigned int);

#endif /*BRANCHING_H_*/

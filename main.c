
#include <stdio.h>
#include <stdlib.h>
#include "orlin.h"

/*
 * Exit Codes:
 * 0:   No Error occured
 * 1:   Error occured while opening or reading file or the given instance is erroneous
 * 2:   Error while allocating memory
 */

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        puts("Usage: ./orlin <LIST OF FILES SEPARATED BY SPACES>");
    }
    else
    {
        argc--;
        while (argc)
        {
            Orlin(*++argv);
            argc--;
        }
    }

    return (0);
}
